package com.company;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        MyStack firstStack = new MyStack();
        firstStack.push("big");
        firstStack.push("small");
        firstStack.push("medium");
        firstStack.push("large");
        firstStack.push("enormous");
        System.out.println(firstStack.get());
        Optional<String> poppedItem = firstStack.pop();
        System.out.println(poppedItem);
        System.out.println(firstStack.get());
    }
}
