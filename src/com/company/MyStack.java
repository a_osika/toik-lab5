package com.company;

import java.util.*;

public class MyStack implements StackOperations {

    private final ArrayList<String> list;

    public MyStack() {
        this.list = new ArrayList<>();
    }

    @Override
    public List<String> get() {
        if (list.isEmpty()) {
            return new ArrayList<>();
        }
        else {
            ArrayList<String> reversedList = new ArrayList<>(list);
            Collections.reverse(reversedList);
            return reversedList;
        }
    }

    @Override
    public Optional<String> pop() {
        if (list.isEmpty()) {
            return Optional.empty();
        }
        else {
            String poppedItem = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            return Optional.of(poppedItem);
        }
    }

    @Override
    public void push(String item) {
        list.add(item);
    }
}